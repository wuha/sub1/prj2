package com.van.nhung.van_nhung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VanNhungApplication {

	public static void main(String[] args) {
		SpringApplication.run(VanNhungApplication.class, args);
	}

}

